package lp.pl.bank.handlers;

import lp.pl.bank.state.Shop;
import lp.pl.bank.state.Transaction;

public class PriceHandler
{

    // Adjusts Shop Price Based on Corresponding Transaction
    public static void updatePrice(Shop shop, Transaction tran)
    {
        shop.setPrice(shop.getPrice() + netPriceChange(shop, tran));
    }

    // Calculates Net Price Change Based on Corresponding Transaction
    public static double netPriceChange(Shop shop, Transaction tran)
    {
        double net;
        net = Math.abs(tran.getTransactionQty()) / shop.getQtyPerCent() * 0.01;
        if (tran.getBuyOrSell().equalsIgnoreCase("buy"))
        {
            return Math.abs(net);
        }
        else
        {
            return -net;
        }
    }
}
