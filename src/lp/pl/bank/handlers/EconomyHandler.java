package lp.pl.bank.handlers;

import lp.pl.bank.Main;
import lp.pl.bank.state.Transaction;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

public class EconomyHandler
{

    static Economy eco = Main.econ;

    public static boolean setupEconomy()
    {
        if (Bukkit.getServer().getPluginManager().getPlugin("Vault") == null)
        {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = (RegisteredServiceProvider<Economy>) Bukkit.getServer().getServicesManager().getRegistration((Class) Economy.class);
        if (rsp == null)
        {
            return false;
        }
        Main.econ = (Economy) rsp.getProvider();
        return Main.econ != null;
    }

    public static void addMoney(Player p, double amt)
    {
        eco.bankDeposit(p.getName(), amt);
    }

    public static void removeMoney(Player p, double amt)
    {
        eco.bankWithdraw(p.getName(), amt);
    }

    public static boolean hasEnough(Transaction sale)
    {
        Player p = sale.getPlayer();
        double price = Math.abs(sale.netProfit());
        double bal = eco.getBalance(p);
        if (bal >= price)
        {
            return true;
        }
        return false;
    }

    public static void executeSale(Transaction sale)
    {
        Player p = sale.getPlayer();
        double amt = Math.abs(sale.netProfit());
        if (sale.getBuyOrSell().equalsIgnoreCase("buy"))
        {
            addMoney(p, amt);
        }
        else
        {
            removeMoney(p, amt);
        }
    }
}
