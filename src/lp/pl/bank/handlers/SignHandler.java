package lp.pl.bank.handlers;

import lp.pl.bank.state.Shop;
import org.bukkit.block.Sign;

import static lp.pl.bank.handlers.ChatHandler.round;

public class SignHandler
{

    // Update Sign with corresponding shop info.
    public static void updateSign(Shop shop)
    {
        Sign sign = (Sign) shop.getLoc().getBlock().getState();
        sign.setLine(2, "Stock: " + shop.getStock());
        sign.setLine(3, "Cost: $" + round(shop.getPrice(), 2));
        sign.update();
    }
}
