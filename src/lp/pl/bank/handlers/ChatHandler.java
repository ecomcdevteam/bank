package lp.pl.bank.handlers;

import lp.pl.bank.state.Shop;
import lp.pl.bank.state.Transaction;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ChatHandler
{

    private static String prefix = ChatColor.AQUA + "[" + ChatColor.GOLD + "EcoShop" + ChatColor.AQUA + "] ";

    // Chat method for successful sale
    public static void onTransactionSuccess(Transaction sale)
    {
        int saleQty = Transaction.getTransactionQty();
        String itemName = Shop.getType().name();
        String cost = "$" + round(Math.abs(Transaction.netProfit()), 2);
        String tax = "$" + round(Math.abs(Transaction.netProfit()), 2) * 0.06;

        if (Transaction.getBuyOrSell().equalsIgnoreCase("buy"))
        {
            saleMsg(sale, ChatColor.GREEN + "You have purchased " + saleQty + " " + itemName + " for " + cost + " plus a sales tax of 6% " + "(" + tax + ")");
        }
        else
        {
            saleMsg(sale, ChatColor.GREEN + "You have sold " + saleQty + " " + itemName + " for " + cost);
        }
    }

    // Chat method for insufficient money
    public static void saleFailureMoney(Transaction sale)
    {
        saleMsg(sale, ChatColor.RED + "You don't have enough money!");
    }

    // Chat method for insufficient inv space
    public static void saleFailureInvSpace(Transaction sale)
    {
        saleMsg(sale, ChatColor.RED + "You don't have enough space in your inventory!");
    }

    // Chat method for insufficient items in inventory
    public static void saleFailureInsufficientItems(Transaction sale)
    {
        saleMsg(sale, ChatColor.RED + "You don't have enough items to sell!");
    }

    // Chat method for insufficient Reserve stock
    public static void saleFailureShopStock(Transaction sale)
    {
        saleMsg(sale, ChatColor.RED + "Out of stock!");
    }

    // Unauthorized shop creation attempt
    public static void createError(Player p)
    {
        prefix(p, ChatColor.RED + "You aren't allowed to create Reserve shops!");
    }

    // Successful shop creation
    public static void createSuccess(Player p)
    {
        prefix(p, ChatColor.GREEN + "Reserve shop created!");
    }

    // Send message related to a Transaction
    public static void saleMsg(Transaction sale, String msg)
    {
        Player p = sale.getPlayer();
        p.sendMessage(prefix + msg);
    }

    // Send message
    public static void prefix(Player p, String m)
    {
        p.sendMessage(prefix + m);
    }

    // Round to X places
    public static double round(double value, int places)
    {
        double intermediateVal = (value * Math.pow(10, places)) - 0.5;
        long roundedVal = Math.round(intermediateVal);
        double returnVal = roundedVal / Math.pow(10, places);

        return returnVal;
    }
}
