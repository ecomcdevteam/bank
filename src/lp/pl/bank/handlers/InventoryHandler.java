package lp.pl.bank.handlers;

import lp.pl.bank.state.Transaction;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryHandler
{

    private final static int STACK_SIZE = 64;

    // Get the total number of items that can fit in a player's inventory
    public static int getInvSpace(Player p, Material m)
    {
        int invSpace = 0;
        ItemStack[] contents = p.getInventory().getStorageContents();

        for (ItemStack s : contents)
        {
            if (s == null)
            {
                invSpace += STACK_SIZE;
            }
            else if (s.getType().equals(m))
            {
                invSpace += STACK_SIZE - s.getAmount();
            }
        }

        return invSpace;
    }

    // Remove X amount of specified items
    public static void removeItems(Player player, ItemStack type, int toRemove)
    {
        Inventory inv = player.getInventory();
        int slot = 0;
        ItemStack retVal;
        while (toRemove > 0)
        {
            while (inv.getItem(slot) == null || !inv.getItem(slot).isSimilar(type))
            {
                ++slot;
            }
            ItemStack stack = inv.getItem(slot);
            retVal = stack.clone();
            retVal.setAmount(toRemove);
            if (stack.getAmount() > toRemove)
            {
                stack.setAmount(stack.getAmount() - toRemove);
                inv.setItem(slot, stack);
                toRemove = 0;
            }
            else
            {
                toRemove -= stack.getAmount();
                inv.removeItem(stack);
            }
        }
    }

    // Add X items to a player's inventory
    private static void addToInv(ItemStack item, Player p, int amount)
    {
        Inventory inv = (Inventory) p.getInventory();
        item.setAmount(amount);
    }

    public static boolean hasItems(Transaction sale)
    {
        Inventory inv = Transaction.getPlayer().getInventory();
        Material m = sale.getShop().getType();
        int amt = sale.getTransactionQty();
        if (inv.containsAtLeast(new ItemStack(m), amt))
        {
            return true;
        }
        return false;
    }

    public static void executeSale(Transaction sale)
    {
        Player p = sale.getPlayer();
        Material m = sale.getShop().getType();
        int amt = sale.getTransactionQty();

        if (sale.getBuyOrSell().equalsIgnoreCase("buy"))
        {
            addToInv(new ItemStack(m), p, amt);
        }
        else
        {
            removeItems(p, new ItemStack(m), amt);
        }
    }

}
