package lp.pl.bank;

import lp.pl.bank.handlers.ChatHandler;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import static lp.pl.bank.handlers.ChatHandler.round;
import static lp.pl.bank.handlers.InventoryHandler.getInvSpace;
import static lp.pl.bank.handlers.InventoryHandler.removeItems;

public class Listeners implements Listener
{
    public static Main plugin;

    public Listeners(Main instance)
    {
        plugin = instance;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onSignPlace(SignChangeEvent e)
    {
        Player p = e.getPlayer();
        if (e.getLine(0).toLowerCase().contains("[reserve]"))
        {
            if (!p.hasPermission("bank.create"))
            {
                ChatHandler.createError(p);
                e.setCancelled(true);
                return;
            }
            if (e.getLine(1).split(" ").length == 2 && isNumeric(e.getLine(1).split(" ")[0]) && isItem(e.getLine(1).split(" ")[1]) && e.getLine(2).isEmpty() && e.getLine(3).isEmpty())
            {
                ItemStack item = null;
                try
                {
                    item = Main.ess.getItemDb().get(e.getLine(1).split(" ")[1]);
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }

                assert item != null;

                String stockS = Main.config.getString(item.getType().name()).split(";")[3];
                String startPriceS = Main.config.getString(item.getType().name()).split(";")[4];
                int stock = Integer.parseInt(stockS);
                int startPrice = Integer.parseInt(startPriceS);
                e.setLine(0, ChatColor.GOLD + "[" + ChatColor.GREEN + "Reserve" + ChatColor.GOLD + "]");
                e.setLine(2, "Stock: " + stock);
                e.setLine(3, "Cost: " + startPrice);
                p.sendMessage(ChatColor.GREEN + "You have created a shop!");
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBreak(BlockBreakEvent e)
    {
        if (e.getBlock().getState() instanceof Sign)
        {
            Sign sign = (Sign) e.getBlock().getState();
            if (ChatColor.stripColor(sign.getLine(0).toLowerCase()).contains("[reserve]") && !e.getPlayer().isSneaking())
            {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onUse(PlayerInteractEvent e)
    {
        Player p = e.getPlayer();
        if ((e.getAction() == Action.LEFT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_BLOCK) && e.getClickedBlock().getState() instanceof Sign)
        {
            Sign sign = (Sign) e.getClickedBlock().getState();
            if (ChatColor.stripColor(sign.getLine(0)).equalsIgnoreCase("[Reserve]"))
            {
                ItemStack item = null;
                try
                {
                    item = Main.ess.getItemDb().get(sign.getLine(1).split(" ")[1]);
                }
                catch (Exception e2)
                {
                    e2.printStackTrace();
                }
                String unitsPerCentS = Main.config.getString(item.getType().name()).split(";")[1];
                String minPriceS = Main.config.getString(item.getType().name()).split(";")[0];
                String maxPriceS = Main.config.getString(item.getType().name()).split(";")[2];
                String stockS = Main.config.getString(item.getType().name()).split(";")[3];
                String startPriceS = Main.config.getString(item.getType().name()).split(";")[4];
                int unitsPerCent = Integer.parseInt(unitsPerCentS);
                int minPrice = Integer.parseInt(minPriceS);
                int maxPrice = Integer.parseInt(maxPriceS);
                int stock = Integer.parseInt(stockS);
                int startPrice = Integer.parseInt(startPriceS);
                int stockOnSign = Integer.parseInt(ChatColor.stripColor(sign.getLine(2).split(" ")[1]));
                double priceOnSign = Double.parseDouble(ChatColor.stripColor(sign.getLine(3)).split(" ")[1]);
                if (e.getAction() == Action.RIGHT_CLICK_BLOCK)
                {
                    if (priceOnSign < minPrice)
                    {
                        sign.setLine(3, "Cost: " + minPrice);
                    }
                    sign.update();
                    int amount = Integer.parseInt(sign.getLine(1).split(" ")[0]);
                    if (e.getPlayer().isSneaking())
                    {
                        if (amount > getInvSpace(p, item.getType()))
                        {
                            amount = getInvSpace(p, item.getType());
                            if (amount == 0)
                            {
//                                ChatHandler.saleFailureInvSpace(p);
                                return;
                            }
                        }
                        else
                        {
                            amount = 32;
                        }
                    }
                    if (!Main.econ.has((OfflinePlayer) p, priceOnSign * amount))
                    {
//                        ChatHandler.saleFailureMoney(p);
                        return;
                    }
                    if (getInvSpace(p, item.getType()) >= amount)
                    {
                        if (stockOnSign - amount >= 0)
                        {
                            item.setAmount(amount);
                            Main.econ.withdrawPlayer((OfflinePlayer) p, priceOnSign * amount);
                            p.getInventory().addItem(new ItemStack[]{item});
                            p.updateInventory();
                            priceOnSign = Double.parseDouble(ChatColor.stripColor(sign.getLine(3)).split(" ")[1]);
                            if (priceOnSign < minPrice)
                            {
                                sign.setLine(3, "Cost: " + minPrice);
                            }
                            priceOnSign = Double.parseDouble(ChatColor.stripColor(sign.getLine(3)).split(" ")[1]);
//                            ChatHandler.onTransactionSuccess(p);
//                            p.sendMessage(String.valueOf(this.prefix) + ChatColor.GREEN + "You have bought " + amount + " " + item.getType().name() + " from the Reserve for $" + priceOnSign * amount + ".");
                            sign.setLine(3, "Cost: " + round(startPrice - (stockOnSign - stock) / unitsPerCent * 0.01, 2));
                            sign.update();
                            sign.setLine(2, "Stock: " + (stockOnSign - amount));
                            sign.update();
                            priceOnSign = Double.parseDouble(ChatColor.stripColor(sign.getLine(3)).split(" ")[1]);
                            if (priceOnSign < minPrice)
                            {
                                sign.setLine(3, "Cost: " + minPrice);
                            }
                            sign.update();
                        }
                        else
                        {
//                            p.sendMessage(String.valueOf(this.prefix) + ChatColor.RED + "Not enough Stock!");
                        }
                    }
                    else
                    {
//                        p.sendMessage(String.valueOf(this.prefix) + ChatColor.RED + "You do not have enough inventory space!");
                    }
                }
                if (e.getAction() == Action.LEFT_CLICK_BLOCK)
                {
                    if (priceOnSign < minPrice)
                    {
                        sign.setLine(3, "Cost: " + minPrice);
                    }
                    sign.update();
                    int amount = Integer.parseInt(sign.getLine(1).split(" ")[0]);
                    if (e.getPlayer().isSneaking())
                    {
                        amount = 32;
                    }
                    if (!p.getInventory().containsAtLeast(item, amount))
                    {
//                        p.sendMessage(String.valueOf(this.prefix) + ChatColor.RED + "You do not have enough items to sell!");
                        return;
                    }
                    double tax = round(priceOnSign * amount * 0.06, 2);
                    item.setAmount(amount);
                    Main.econ.depositPlayer((OfflinePlayer) p, priceOnSign * amount - tax);
                    removeItems(p, item, amount);
                    sign.setLine(3, "Cost: " + round(startPrice - (stockOnSign - stock) / unitsPerCent * 0.01, 2));
                    sign.update();
                    sign.setLine(2, "Stock: " + (stockOnSign + amount));
                    sign.update();
//                    p.sendMessage(String.valueOf(this.prefix) + ChatColor.GREEN + "You have sold " + amount + " " + item.getType().name() + " to the Reserve for $" + priceOnSign * amount + " minus a $" + tax + " Stocking fee.");
                    priceOnSign = Double.parseDouble(ChatColor.stripColor(sign.getLine(3)).split(" ")[1]);
                    if (priceOnSign < minPrice)
                    {
                        sign.setLine(3, "Cost: " + minPrice);
                    }
                    sign.update();
                }
            }
        }
    }

    public static boolean isNumeric(String str)
    {
        try
        {
            Double.parseDouble(str);
        }
        catch (NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    public static boolean isItem(String str)
    {
        try
        {
            Main.ess.getItemDb().get(str);
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }


}
