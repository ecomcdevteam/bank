package lp.pl.bank.state;

import lp.pl.bank.handlers.ChatHandler;
import lp.pl.bank.handlers.EconomyHandler;
import lp.pl.bank.handlers.InventoryHandler;
import lp.pl.bank.handlers.SignHandler;
import org.bukkit.entity.Player;

public class Transaction
{

    Player player;
    int transactionQty;
    Shop shop;
    String buyOrSell;
    final int HALF_STACK = 32;

    public Transaction(Player p, int qty, Shop shop, String buyOrSell)
    {
        player = p;
        transactionQty = qty;
        this.shop = shop;
        this.buyOrSell = buyOrSell;
    }

    // Get & Set Methods
    public Player getPlayer()
    {
        return player;
    }

    public void setPlayer(Player p)
    {
        player = p;
    }

    public int getTransactionQty()
    {
        return transactionQty;
    }

    public void setTransactionQty(int transactionQty)
    {
        this.transactionQty = transactionQty;
    }

    public Shop getShop()
    {
        return shop;
    }

    public void setShop(Shop shop)
    {
        this.shop = shop;
    }

    public String getBuyOrSell()
    {
        return buyOrSell;
    }

    public void setBuyOrSell(String buyOrSell)
    {
        this.buyOrSell = buyOrSell;
    }

    // Net Amount of Items Transacted
    public int netQty()
    {
        int net;
        if (getBuyOrSell().equalsIgnoreCase("buy"))
        {
            net = shop.getStock() + getTransactionQty();
        }
        else
        {
            net = shop.getStock() - getTransactionQty();
        }
        return net;
    }

    // Net Amount of Money Transacted
    public double netProfit()
    {
        double net;
        net = shop.getPrice() * netQty();
        return net;
    }

    // Qty to be pruchased (half stack or single)
    public int getQtyToBeSold(Player p)
    {
        int qty = 1;
        if (p.isSneaking())
        {
            qty = HALF_STACK;
        }
        return qty;
    }

    // Method To execute a transaction
    public void execute(Transaction sale)
    {
        // Execute Reserve Buy
        if (getBuyOrSell().equalsIgnoreCase("buy"))
        {
            if (InventoryHandler.getInvSpace(player, shop.getType()) >= transactionQty)
            {
                if (EconomyHandler.hasEnough(sale))
                {
                    EconomyHandler.executeSale(sale);
                    InventoryHandler.executeSale(sale);
                    SignHandler.updateSign(sale.getShop());
                    ChatHandler.onTransactionSuccess(sale);
                }
                else
                {
                    ChatHandler.saleFailureMoney(sale);
                }
            }
            else
            {
                ChatHandler.saleFailureInvSpace(sale);
            }
        }

        // Execute Reserve Sell
        else
        {
            if (InventoryHandler.hasItems(sale))
            {
                EconomyHandler.executeSale(sale);
                InventoryHandler.executeSale(sale);
                SignHandler.updateSign(sale.getShop());
                ChatHandler.onTransactionSuccess(sale);
            }
            else
            {
                ChatHandler.saleFailureInsufficientItems(sale);
            }
        }

    }

}
