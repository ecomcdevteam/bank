package lp.pl.bank.state;

import org.bukkit.Location;
import org.bukkit.Material;

public class Shop
{

    public static Material itemType;
    public static double price;
    public static int stockQty;
    public static int unitsPerCent;
    public static Location loc;


    Shop(Material type, double cost, int stock, int perCent, Location location)
    {
        itemType = type;
        price = cost;
        stockQty = stock;
        unitsPerCent = perCent;
        loc = location;
    }

    // Get Methods ==========================
    public static Material getType()
    {
        return itemType;
    }

    public double getPrice()
    {
        return price;
    }

    public static int getStock()
    {
        return stockQty;
    }

    public int getQtyPerCent()
    {
        return unitsPerCent;
    }

    public static Location getLoc()
    {
        return loc;
    }

    // Set Methods ===========================
    public static void setType(Material type)
    {
        itemType = type;
    }

    public void setPrice(double cost)
    {
        price = cost;
    }

    public static void setStock(int qty)
    {
        stockQty = qty;
    }

    public static void setQtyPerCent(int perCent)
    {
        unitsPerCent = perCent;
    }

    public static void setLoc(Location loc)
    {
        Shop.loc = loc;
    }


}
