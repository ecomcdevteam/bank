package lp.pl.bank;

import lp.pl.bank.handlers.EconomyHandler;
import org.bukkit.plugin.java.*;
import org.bukkit.configuration.file.*;

import com.earth2me.essentials.*;
import net.milkbowl.vault.economy.*;

import org.bukkit.event.*;

public class Main extends JavaPlugin
{
    public static FileConfiguration config;
    public static Essentials ess;
    public static Main instance;
    public static Economy econ = null;

    public void onEnable()
    {
        this.saveDefaultConfig();
        Main.config = this.getConfig();
        Main.ess = (Essentials) this.getServer().getPluginManager().getPlugin("Essentials");
        Main.instance = this;
        this.getServer().getPluginManager().registerEvents((Listener) new Listeners(this),this);
        if (!EconomyHandler.setupEconomy())
        {
            this.getServer().getPluginManager().disablePlugin(this);
        }
    }

    public void onDisable()
    {
    }
}
